﻿using PlanetLibrary.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlanetSystemView
{
    public partial class MainWindow : Form
    {
        public static SystemState _planetSystem;

        private const double G = 0.000017;
        private const double DT = 0.001;
        private const double DH = 0.00000000001;
        public static double WindowScale = 20;

        private const double PlanetScale = 1000;
        private const double SunScale = 10;
        private const int ProjectionSize = 400;
        public MainWindow()
        {
            InitializeComponent();
            _planetSystem = new SystemState
            {
                Planets = new List<Planet> {
                new Planet { Mass= 3332940, Name="Sun", R=0.00436*SunScale },
                new Planet { Mass= 1, Name="Earth", Coordinates=new Point3d{ X=1 },R=0.00004*PlanetScale, SpeedVector=new Point3d{ Y= 30 / 5 } },
                new Planet { Mass= .107, Name="Mars", Coordinates=new Point3d{ X=1.5233 }, R=0.00002*PlanetScale, SpeedVector=new Point3d{ Y=25 / 5 } },
                new Planet { Mass= .815, Name="Venus", Coordinates=new Point3d{ X=.723 }, R=0.00004*PlanetScale, SpeedVector=new Point3d{ Y= 35 / 5 } },
                new Planet { Mass= 317.8, Name="Jupiter", Coordinates=new Point3d{ X=5.2 }, R=0.0004*PlanetScale, SpeedVector=new Point3d{ Y=13/5 } },
                new Planet { Mass=95.16, Name="Saturn", Coordinates=new Point3d{ X=10}, R=0.0003*PlanetScale, SpeedVector=new Point3d{ Y=10/5 }}
                }
            };
            _planetSystem.DH = DH;
            _planetSystem.G = G;
            var xPersp = new Perspective(x => x.X, x => x.Y)
            {
                Width = ProjectionSize,
                Height = ProjectionSize
            };
            var yPersp = new Perspective(x => x.Y, x => x.Z)
            {
                Width = ProjectionSize,
                Height = ProjectionSize,
                Location = new Point { X = ProjectionSize }
            };
            var zPersp = new Perspective(x => x.X, x => x.Z)
            {
                Width = ProjectionSize,
                Height = ProjectionSize,
                Location = new Point { Y = ProjectionSize }
            };
            Controls.Add(xPersp);
            Controls.Add(yPersp);
            Controls.Add(zPersp);
            Size = new Size { Height = 2 * ProjectionSize, Width = 2 * ProjectionSize };
            //Controls.Add(new Perspective(x => x.X, x => x.Y) { Width = Width - 2, Height = Height - 2, Location = new Point { X = 1, Y = 1 } });
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            _planetSystem.CalculateNextState(DT);
            base.OnPaint(e);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
