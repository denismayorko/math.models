﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PlanetLibrary.Engine;

namespace PlanetSystemView
{
    public partial class Perspective : UserControl
    {
        private readonly Func<Point3d, double> _selector1;
        private readonly Func<Point3d, double> _selector2;
        public Perspective(Func<Point3d, double> selector1, Func<Point3d, double> selector2)
        {
            InitializeComponent();
            _selector1 = selector1;
            _selector2 = selector2;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            var graphics = e.Graphics;
            var bounds = e.Graphics.VisibleClipBounds;
            var xyXScale = bounds.Width / MainWindow.WindowScale;
            foreach (var planet in MainWindow._planetSystem.Planets)
                using (var pen = new Pen(planet.Name == "Sun" ? Color.Red : Color.Black))
                    graphics.DrawEllipse(pen, new RectangleF
                    {
                        X = (float)(bounds.Width / 2 - planet.R * xyXScale + _selector1(planet.Coordinates) * xyXScale),
                        Y = (float)(bounds.Height / 2 - planet.R * xyXScale + _selector2(planet.Coordinates) * xyXScale),
                        Width = (float)(2 * planet.R * xyXScale),
                        Height = (float)(2 * planet.R * xyXScale)
                    });
            base.OnPaint(e);
        }
    }
}
