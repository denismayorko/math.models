﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveEquation
{
    public class Calculator
    {
        public void Calculate(ref MathModel model)
        {
            for (var i = 1; i < model.N - 1; i++)
                model.G[i] += model.V * model.V * 
                    (model.State[i + 1] - 2 * model.State[i] + model.State[i - 1]) * 
                    model.DT / model.H / model.H;
            model.State[0] = 0;
            model.State[model.N-1] = 0;
            for (var i = 1; i < model.N - 1; i++)
                model.State[i] += model.G[i] * model.DT;
        }
    }
}
