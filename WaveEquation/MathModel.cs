﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveEquation
{
    public class MathModel
    {
        public int N { get; set; }
        public float V { get; set; }
        public float DT { get; set; }
        public float H { get; set; }

        public float[] State { get; set; }
        public float[] G { get; set; }
        public float DX { get; set; }
    }
}
