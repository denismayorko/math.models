﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaveEquation
{
    public partial class MainWindow : Form
    {
        private MathModel Model;
        private readonly int _stepNums = 10000;
        private List<float[]> SavedResults { get; set; }
        private readonly Calculator _calculator;
        private int IterationsNumber;
        public MainWindow()
        {
            InitializeComponent();
            _calculator = new Calculator();
            SetValues();
        }
        private void TrackBarValueChanged(object sender, EventArgs e)
        {
            SetValues();
        }
        private void SetValues()
        {
            var oldIterationNumber = IterationsNumber;
            Model = new MathModel
            {
                N = trackBar1.Value * 20,
                V = trackBar2.Value,
                DT = trackBar3.Value / 100f,
                H = trackBar4.Value,
                DX = trackBar6.Value
            };
            Model.State = new float[Model.N];
            Model.G = new float[Model.N];
            Model.State[0] = Model.DX;
            IterationsNumber = trackBar5.Value;
            if (IterationsNumber != oldIterationNumber) Show(IterationsNumber);
            else Recalculate();
        }
        private void Recalculate()
        {
            SavedResults = new List<float[]>();
            for (int i = 0; i <= _stepNums; i++)
            {
                _calculator.Calculate(ref Model);
                SavedResults.Add(new float[Model.N]);
                Array.Copy(Model.State, 0, SavedResults[i], 0, Model.N);
            }
            Show(IterationsNumber);

        }
        private void Show(int index)
        {
            chart1.Series[0].Points.DataBind(Enumerable.Range(0, SavedResults[index].Length).Select(i => new PointF(i, SavedResults[index][i])).ToArray(), "X", "Y", string.Empty);
        }
    }
}
