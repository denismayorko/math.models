﻿using System.Diagnostics;

namespace PlanetLibrary.Engine
{
    [DebuggerDisplay("Name = {Name}")]
    public class Planet
    {
        public double Mass { get; set; }
        public Point3d Coordinates { get; set; }
        public Point3d SpeedVector { get; set; }
        public Point3d AccelerationVector { get; set; }
        public string Name { get; set; }
        public double R { get; set; }
        public Planet()
        {
            Name = string.Empty;
        }
    }
}