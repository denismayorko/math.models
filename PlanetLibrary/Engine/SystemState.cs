﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlanetLibrary.Engine
{
    public class SystemState
    {
        public double G { get; set; }
        public double DH { get; set; }
        public List<Planet> Planets { get; set; }
        public SystemState()
        {
            Planets = new List<Planet>();
        }
        public void CalculateNextState(double dt)
        {
            var fMatrix = new Point3d[Planets.Count, Planets.Count];
            for (int i = 0; i < Planets.Count; i++)
                for (int j = i; j < Planets.Count; j++)
                {
                    if (i == j) continue;
                    var uVector = new Point3d
                    {
                        X = CalculateDU(Planets[i], Planets[j], vector => vector.X),
                        Y = CalculateDU(Planets[i], Planets[j], vector => vector.Y),
                        Z = CalculateDU(Planets[i], Planets[j], vector => vector.Z)
                    };
                    fMatrix[i, j] = uVector * -1;
                    fMatrix[j, i] = uVector;
                }
            for (int i = 0; i < Planets.Count; i++)
                Planets[i].AccelerationVector = Enumerable.Range(0, fMatrix.GetLength(1)).Select(x => fMatrix[x, i]).Aggregate((x, y) => x + y) / Planets[i].Mass;

            for (int i = 0; i < Planets.Count; i++)
                Planets[i].SpeedVector += Planets[i].AccelerationVector * dt;

            for (int i = 0; i < Planets.Count; i++)
                Planets[i].Coordinates += Planets[i].SpeedVector * dt + Planets[i].AccelerationVector * dt * dt / 2;
        }
        private double CalculateDU(Planet planet0, Planet planet1, Func<Point3d, double> difParam)
        {
            var rDiff = planet1.Coordinates - planet0.Coordinates;
            var rSquare = rDiff ^ 2;
            var rSquareCoordsSum = rSquare.X + rSquare.Y + rSquare.Z;
            var firstVal = Math.Sqrt(rSquareCoordsSum + difParam(rDiff) * DH + DH * DH);
            var secVal = Math.Sqrt(rSquareCoordsSum);
            var result = G * planet1.Mass * planet0.Mass * (firstVal - secVal) / firstVal / secVal / DH;
            return result;
        }
    }
}
