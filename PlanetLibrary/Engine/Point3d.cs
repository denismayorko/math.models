﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PlanetLibrary.Engine
{
    [DebuggerDisplay("({X},{Y},{Z})")]
    public struct Point3d
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public static Point3d operator ^(Point3d point0, int power)
        {
            var result = new Point3d
            {
                X = Math.Pow(point0.X, power),
                Y = Math.Pow(point0.Y, power),
                Z = Math.Pow(point0.Z, power)
            };
            return result;
        }
        public static Point3d operator -(Point3d point0, Point3d point1)
        {
            var result = new Point3d
            {
                X = point0.X - point1.X,
                Y = point0.Y - point1.Y,
                Z = point0.Z - point1.Z
            };
            return result;
        }
        public static Point3d operator +(Point3d point0, Point3d point1)
        {
            var result = new Point3d
            {
                X = point0.X + point1.X,
                Y = point0.Y + point1.Y,
                Z = point0.Z + point1.Z
            };
            return result;
        }
        public static Point3d operator *(Point3d point0, Point3d point1)
        {
            var result = new Point3d
            {
                X = point0.X * point1.X,
                Y = point0.Y * point1.Y,
                Z = point0.Z * point1.Z
            };
            return result;
        }
        public static Point3d operator *(Point3d point0, double value)
        {
            var result = new Point3d
            {
                X = point0.X * value,
                Y = point0.Y * value,
                Z = point0.Z * value
            };
            return result;
        }
        public static Point3d operator /(Point3d point0, double value)
        {
            var result = new Point3d
            {
                X = point0.X / value,
                Y = point0.Y / value,
                Z = point0.Z / value
            };
            return result;
        }
    }
}
