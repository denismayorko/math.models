﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathModeling
{
    public partial class MainWindow : Form
    {
        private readonly MathModel _model;
        private readonly Calculator _calculator;
        public MainWindow()
        {
            _model = new MathModel();
            _calculator = new Calculator();
            InitializeComponent();
            ResetValues();
            Recalculate();
        }
        private void ResetValues()
        {
            _model.PBirthRate = trackBar1.Value / 100f;
            _model.NBirthRate = trackBar2.Value / 100f;
            _model.PDeathRate = trackBar6.Value / 100f;
            _model.HuntResult = trackBar8.Value / 100f;
            _model.N = trackBar7.Value;
            _model.P = trackBar5.Value;
            _model.ColumnCount = (int)Math.Pow(10,trackBar3.Value)*3;
            Recalculate();
        }
        private void TrackBarValueChanged(object sender, EventArgs e) => ResetValues();

        private void Recalculate()
        {
            var result = _calculator.Calculate(_model);
            chart1.Series[0].Points.DataBind(Enumerable.Range(0, result.GetLength(1)).Select(x => new PointF(x, result[0, x])).ToArray(), "X", "Y", string.Empty);
            chart1.Series[1].Points.DataBind(Enumerable.Range(0, result.GetLength(1)).Select(x => new PointF(x, result[1, x])).ToArray(), "X", "Y", string.Empty);
        }
    }
}
