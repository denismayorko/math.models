﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathModeling
{
    public class Calculator
    {
        public float[,] Calculate(MathModel model)
        {
            var result = new float[2, model.ColumnCount];
            var currentState = model;
            for (var i = 0; i < model.ColumnCount; i++)
            {
                var nextState = new MathModel
                {
                    N = currentState.N + ((currentState.N * currentState.NBirthRate - currentState.HuntResult * currentState.N * currentState.P) * model.DT),
                    P = currentState.P + ((currentState.PBirthRate * currentState.N * currentState.P - currentState.P * currentState.PDeathRate) * model.DT),
                    HuntResult = currentState.HuntResult,
                    NBirthRate = currentState.NBirthRate,
                    PBirthRate = currentState.PBirthRate,
                    PDeathRate = currentState.PDeathRate
                };
                result[0, i] = currentState.N;
                result[1, i] = currentState.P;
                currentState = nextState;
            }
            return result;
        }
    }
}
