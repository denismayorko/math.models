﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathModeling
{
    public class MathModel
    {
        private float n;
        private float p;
        public float N { get => Math.Max(0, n); set => n = value; }
        public float P { get => Math.Max(0, p); set => p = value; }
        public float NBirthRate { get; set; }
        public float PBirthRate { get; set; }
        public float PDeathRate { get; set; }
        public float HuntResult { get; set; }
        public float DT { get; set; } = 1;
        public int ColumnCount { get; set; } = 10000;
    }
}
